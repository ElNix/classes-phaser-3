//La variable "game" est déclarée globale dans BoiteAffichage

class EcranDialogue {
    private texte: string;
    private sceneToAttach: any;
    private nomIconeParleur: string;
    private nomFond: string;
	private boiteAffichage: BoiteAffichage;
	private largeur = game.config.width*0.9;
	private hauteur = game.config.height*0.5;
	private conteneurTexte: any;
	private conteneurIcone: any;

	constructor($texte: string, $sceneToAttach: any, $nomIconeParleur: string, $nomFond: string="noir") {
		this.texte = ajoutRetoursChariot($texte);
		this.sceneToAttach = $sceneToAttach;
		this.nomIconeParleur = $nomIconeParleur;
		this.nomFond = $nomFond;
	}

	public creerEtAfficher(): void {
		this.boiteAffichage = new BoiteAffichage(this.sceneToAttach,50,50,this.largeur,this.hauteur,this.nomFond);
		this.boiteAffichage.creerEtAfficher();
		this.conteneurIcone = this.sceneToAttach.add.sprite(150,150, this.nomIconeParleur).setOrigin(0.5);
		this.conteneurTexte = this.sceneToAttach.add.text(100+(this.largeur/2),150, this.texte, { fontSize: '27px', fill: '#fff' }).setOrigin(0.5);
	}
	
	public setDialogue(texte: string, icone: string) {
		this.texte = ajoutRetoursChariot(texte);
		this.nomIconeParleur = icone;
		this.conteneurTexte.setText(this.texte);
		this.conteneurIcone.setTexture(this.nomIconeParleur);
	}

	public getZoneReaction(): any {
		return this.boiteAffichage.getZoneReaction();
	}
	
	public fermer(): void {
		this.conteneurIcone.destroy();
		this.conteneurTexte.destroy();
		this.boiteAffichage.fermer();
	}


}