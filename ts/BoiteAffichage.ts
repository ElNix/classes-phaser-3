declare let game: any; //Indique que la variable "game" est globale

class BoiteAffichage {
    protected sceneToAttach: any;
    protected x: number;
    protected y: number;
    protected largeur: number;
    protected hauteur: number;
    private nomFond: string;
    private zoneReaction: any;

	constructor($sceneToAttach: any, $x: number=game.config.width*0.5-150, $y: number=game.config.height*0.2-50, $largeur: number=300, $hauteur: number=100, $nomFond: string="noir") {
		this.sceneToAttach = $sceneToAttach;
		this.x = $x;
		this.y = $y;
		this.largeur = $largeur;
		this.hauteur = $hauteur;
    this.nomFond = $nomFond;
  }
  
  public creerEtAfficher(): void {
    this.zoneReaction = this.sceneToAttach.add.sprite(this.x+this.largeur/2, this.y+this.hauteur/2, this.nomFond).setOrigin(0.5).setInteractive();
    this.zoneReaction.displayWidth = this.largeur;
    this.zoneReaction.displayHeight = this.hauteur;
   }

  public fermer(): void {
    this.zoneReaction.destroy();
  }

  public getZoneReaction(): any {
    return this.zoneReaction;
  }

}
