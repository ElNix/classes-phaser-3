class BoiteTexte extends BoiteAffichage {

    private texte: string;
    private conteneurTexte: any;
    private centre: boolean;


	constructor($texte: string,$sceneToAttach: any,$centre: boolean = true, $x: number, $y: number, $largeur: number, $hauteur: number, $nomFond: string="noir") {
        super($sceneToAttach, $x, $y, $largeur, $hauteur, $nomFond);
        this.texte = $texte;
        this.centre = $centre;
	}

    public creerEtAfficher(): void {
        super.creerEtAfficher();
        if(this.centre)
            this.conteneurTexte = this.sceneToAttach.add.text(this.x+this.largeur/2, this.y+this.hauteur/2, this.texte, { fontSize: '27px', fill: '#fff' }).setOrigin(0.5);
        else
            this.conteneurTexte = this.sceneToAttach.add.text(this.x+20, this.y+10, this.texte, { fontSize: '27px', fill: '#fff' });
        
    }

    public fermer(): void {
        super.fermer();
        this.conteneurTexte.destroy();
      }

    public getTexte(): string {
        return this.texte;
    }

    public setTexte(value: string) {
        this.texte = value;
        this.conteneurTexte.setText(this.texte);
    }

}

/* UTILISATION

Necessité d'avoir preload un fond en tant qu'image

Dans un "create" de scene :

this.ajoutDialogues(Main);

Dans une fonction de la scene :

ajoutDialogues: function(scene) {

        this.boite1 = new BoiteTexte("Message 1", this);
        this.boite1.creerEtAfficher();

        this.compteurDialogue = 1;

        this.boite1.getZoneReaction().on('pointerdown', function (pointer) {
            switch(scene.compteurDialogue)
            {
                case 1 :
                    scene.boite1.setTexte("Deuxieme Message");
                    break;
                case 2 : 
                    scene.boite1.setTexte("Message le 3e");
                    break;
            }
            scene.compteurDialogue++;
        });
        
    }

  */