﻿var Main = new Phaser.Class({

    Extends: Phaser.Scene,

    initialize:

        function Main() {
            Phaser.Scene.call(this, { key: 'Main' });
        },

    preload: function () {
        this.load.image('noir', 'assets/noir.png');

        this.load.image('sonic', 'assets/sonicIcon.png');
        this.load.image('tails', 'assets/tailsIcon.png');
        this.load.image('knuckles', 'assets/knucklesIcon.png');
        this.load.image('fond', 'assets/fond.jpg');
    },

    create: function () {

        Main = this.scene.get("Main");

        this.score = 0;
        this.timedEvent;
        this.sprite;
        this.bar;
        this.maxScore = 200;


        this.add.image(0, 0, 'fond').setOrigin(0, 0);

        this.ajoutDialogues(Main);

    },

    launchingGame: function() {

        this.boiteTexte = new BoiteTexte("Clic Sonic !", this);
        //this.boiteTexte = new BoiteTexte("Clic Sonic !", this, false);
        this.boiteTexte.creerEtAfficher();

        this.bar = new HealthBar(this, 300, 200, 200, 50, this.maxScore);

        this.sprite = this.add.sprite(400, 400, 'sonic').setOrigin(0.5).setInteractive();

        this.sprite.on('pointerdown', function (pointer) {

            this.setTint(0xff0000);
            Main.score += 30;
            Main.majScore();
        });

        this.sprite.on('pointerout', function (pointer) {

            this.clearTint();

        });

        this.sprite.on('pointerup', function (pointer) {

            this.clearTint();

        });

        this.timedEvent = this.time.addEvent({ delay: 1000, callback: this.onEvent, callbackScope: this, loop: true });

    },

    update: function() {

        if (this.score >= this.maxScore) {
            this.timedEvent.remove();
    
            this.boiteTexte.setTexte("VICTOIRE");
            this.sprite.off('pointerdown');
    
        }
    },

    ajoutDialogues: function(scene) {

    /* TEST BOITE AFFICHAGE 

        this.boite = new BoiteAffichage(this,50,50,500,100);
        this.boite.creerEtAfficher();

    */

    /* TEST BOITE TEXTE
        //this.boite1 = new BoiteTexte("Message 1", this);
        this.boite1 = new BoiteTexte("Bonjour \nAvec saut de ligne",this,50,50,500,100);
        this.boite1.creerEtAfficher();

        this.compteurDialogue = 1;

        this.boite1.getZoneReaction().on('pointerdown', function (pointer) {
            switch(scene.compteurDialogue)
            {
                case 1 :
                    scene.boite1.setTexte("Deuxieme Message");
                    break;
                case 2 : 
                    scene.boite1.setTexte("Message the Third");
                    break;
                case 3 :
                    scene.boite1.fermer();
                    break;
            }
            scene.compteurDialogue++;
        });
    */

    //TEST ECRAN DIALOGUE
        this.ecran = new EcranDialogue("Bonjour, c'est Sonic ! Les méchant viennent encore nous embéter !",this, "sonic");
        this.ecran.creerEtAfficher();
    
        this.compteurDialogue = 1;

        this.ecran.getZoneReaction().on('pointerdown', function (pointer) {
            switch(scene.compteurDialogue)
            {
                case 1 :
                    scene.ecran.setDialogue("Et moi c'est Tails !","tails");
                    break;
                case 2 : 
                    scene.ecran.setDialogue("Partons à l'aventure !","knuckles");
                    break;
                case 3 : 
                    scene.ecran.setDialogue("C'est parti !","sonic");
                    break;
                case 4 :
                    scene.ecran.fermer();
                    Main.launchingGame();
                    break;
            }
            scene.compteurDialogue++;
        });

    }, 
    
    onEvent: function() {
        this.score -= 50;
        this.majScore();
    },

    majScore: function() {
        console.log(this.score);
        this.bar.newAmount(this.score);
    }
});

var config = {
    type: Phaser.AUTO,
    width: 800,
    height: 600,
    scene: [
        Main
    ]
};
var game = new Phaser.Game(config);